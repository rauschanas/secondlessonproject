﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System.IO;
using System.Linq;

namespace FileExplorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int ElementMinimumHeight = 50;
        private const int ElementMinimumWidth = 200;
        private const string BackUpTheHierarchy = "···";

        private readonly List<Label> _labels;

        private string _currentPath;
        private int _focusedElementIndex;

        public MainWindow()
        {
            _labels = new List<Label>();
            _currentPath = string.Empty;
            _focusedElementIndex = -1;
            Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
            InitializeComponent();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            KeyDown += MainWindow_KeyDown;
            ShowElements();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            KeyDown -= MainWindow_KeyDown;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    ShowElements();
                    break;
                case Key.Up:
                    SetFocus(_focusedElementIndex - 1);
                    break;
                case Key.Down:
                    SetFocus(_focusedElementIndex + 1);
                    break;
            }
        }

        private string GetPath()
        {
            Label focusedLabel = GetFocusedLabel();
            if (focusedLabel != null)
            {
                string focusedLabelContent = focusedLabel.Content.ToString();
                if (focusedLabelContent == BackUpTheHierarchy)
                {
                    return Directory.GetParent(_currentPath)?.FullName;
                }
                else
                {
                    return Path.Combine(_currentPath, focusedLabelContent);
                }
            }
            return null;
        }

        private string[] GetElementNames(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return Directory.GetLogicalDrives();
            }
            else
            {
                if (File.Exists(path))
                {
                    return Array.Empty<string>();
                }
                else
                {
                    return Directory.GetDirectories(path)
                        .Concat(Directory.GetFiles(path))
                        .Select(p => Path.GetFileName(p))
                        .Prepend(BackUpTheHierarchy)
                        .ToArray();
                }
            }
        }

        private void ClearElements()
        {
            ExplorerGrid.Children.Clear();
            foreach (var label in _labels)
            {
                label.MouseDown -= Label_MouseDown;
                label.MouseDoubleClick -= Label_MouseDoubleClick;
            }
            _labels.Clear();
            ExplorerGrid.RowDefinitions.Clear();
            ExplorerGrid.ColumnDefinitions.Clear();
        }

        private void ShowElements()
        {
            string path = GetPath();

            string[] elementNames = GetElementNames(path);

            ClearElements();

            int columnCount = ExplorerGrid.ActualWidth > ElementMinimumWidth
                ? Convert.ToInt32(ExplorerGrid.ActualWidth) / ElementMinimumWidth
                : 1;
            int rowCount = elementNames.Length / columnCount + 1;

            for (int i = 0; i < columnCount; i++)
            {
                ExplorerGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int i = 0; i < rowCount; i++)
            {
                ExplorerGrid.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < elementNames.Length; i++)
            {
                var label = new Label
                {
                    Height = ElementMinimumHeight,
                    Width = ElementMinimumWidth,
                    Content = elementNames[i],
                    Focusable = true
                };
                label.MouseDown += Label_MouseDown;
                label.MouseDoubleClick += Label_MouseDoubleClick;
                _labels.Add(label);
                ExplorerGrid.Children.Add(label);
                Grid.SetRow(label, i / columnCount);
                Grid.SetColumn(label, i % columnCount);
            }

            _focusedElementIndex = -1;
            _currentPath = path ?? string.Empty;
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowElements();
        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SetFocus(_labels.IndexOf(sender as Label));
        }

        private void SetFocus(int index)
        {
            if (_focusedElementIndex != index && index >= 0 && index < _labels.Count)
            {
                var focusedLabel = GetFocusedLabel();
                if (focusedLabel != null)
                {
                    focusedLabel.Background = Brushes.White;
                }
                _labels[index].Background = Brushes.LightBlue;
                _focusedElementIndex = index;
            }
        }

        private Label GetFocusedLabel()
        {
            return _labels.Count > 0 && _focusedElementIndex > -1
                ? _labels[_focusedElementIndex]
                : null;
        }
    }
}
